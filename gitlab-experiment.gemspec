# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/experiment/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab-experiment'
  spec.version = Gitlab::Experiment::VERSION
  spec.authors = ['GitLab']
  spec.email = ['gitlab_rubygems@gitlab.com']
  spec.summary = 'GitLab experimentation library.'
  spec.homepage = 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment'
  spec.license = 'MIT'
  spec.files = Dir['lib/{generators,gitlab}/**/*'] + ['LICENSE.txt', 'README.md']
  spec.required_ruby_version = '>= 3.0'

  spec.add_runtime_dependency 'activesupport', '>= 3.0'
  spec.add_runtime_dependency 'request_store', '>= 1.0'

  spec.add_development_dependency 'gitlab-dangerfiles', '~> 4.1.0'
  spec.add_development_dependency 'gitlab-styles', '~> 12.0.1'
  spec.add_development_dependency 'lefthook', '~> 1.4.7'
  spec.add_development_dependency 'pry-byebug', '~> 3.10.1'
  spec.add_development_dependency 'rubocop-rails', '~> 2.24.0'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.27.1'

  # tests group
  spec.add_development_dependency 'flipper', '~> 0.26.2'
  spec.add_development_dependency 'generator_spec', '~> 0.9.4'
  spec.add_development_dependency 'rspec-parameterized-table_syntax', '~> 1.0.0'
  spec.add_development_dependency 'rspec-rails', '~> 6.0.3'
  spec.add_development_dependency 'simplecov-cobertura', '~> 2.1.0'
end
