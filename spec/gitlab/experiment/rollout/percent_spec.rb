# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout::Percent do
  let(:subject_experiment) { Gitlab::Experiment.new(:example) }
  let(:counts) { Hash.new(0) }

  before do
    stub_experiments(example: true)

    subject_experiment.variant(:variant1) {}
    subject_experiment.variant(:variant2) {}
  end

  describe "validation" do
    it "validates array type distribution rules with the behaviors count" do
      subject_experiment.rollout(described_class, distribution: [32, 25, 43])

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        "the distribution rules don't match the number of behaviors defined"
      )
    end

    it "validates hash type distribution rules with the variant count" do
      subject_experiment.rollout(described_class, distribution: { variant1: 100 })

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        "the distribution rules don't match the number of behaviors defined"
      )
    end

    it "validates that the distribution rules are a known type" do
      subject_experiment.rollout(described_class, distribution: :foo)

      expect { subject_experiment.assigned.name }.to raise_error(
        Gitlab::Experiment::InvalidRolloutRules,
        'unknown distribution options type'
      )
    end

    context "when distribution is specified as an array" do
      it "validates the total is 100%" do
        subject_experiment.variant(:variant3) {}
        subject_experiment.rollout(described_class, distribution: [33, 33, 33])

        expect { subject_experiment.assigned.name }.to raise_error(
          Gitlab::Experiment::InvalidRolloutRules,
          'the distribution percentages should add up to 100'
        )
      end

      it "forbids negative percentages" do
        subject_experiment.rollout(described_class, distribution: [-10, 100])

        expect { subject_experiment.assigned.name }.to raise_error(
          Gitlab::Experiment::InvalidRolloutRules,
          'the distribution percentage cannot be negative'
        )
      end
    end

    context "when distribution is specified as an hash" do
      it "validates the total is 100%" do
        subject_experiment.variant(:variant3) {}
        subject_experiment.rollout(described_class, distribution: { control: 33, variant1: 33, variant2: 33 })

        expect { subject_experiment.assigned.name }.to raise_error(
          Gitlab::Experiment::InvalidRolloutRules,
          'the distribution percentages should add up to 100'
        )
      end

      it "validates that there are no negative percentages" do
        subject_experiment.rollout(described_class, distribution: { variant1: -10, variant2: 100 })

        expect { subject_experiment.assigned.name }.to raise_error(
          Gitlab::Experiment::InvalidRolloutRules,
          'the distribution percentage cannot be negative'
        )
      end
    end
  end

  context "with default distribution" do
    before do
      subject_experiment.rollout(described_class)
    end

    it "handles when there are no behaviors" do
      allow(subject_experiment).to receive(:variant_names).and_return([])

      expect { run_cycle(subject_experiment) }.not_to raise_error
    end

    it "rolls out relatively evenly to 2 behaviors" do
      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 45, variant2: 55)
    end

    it "rolls out relatively evenly to 3 behaviors" do
      subject_experiment.variant(:variant3) {}

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 34, variant2: 37, variant3: 29)
    end
  end

  context "when distribution is specified as an array" do
    it "rolls out with the expected distribution" do
      subject_experiment.rollout(described_class, distribution: [32, 25, 43])
      subject_experiment.variant(:variant3) {}

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 25, variant2: 27, variant3: 48)
    end

    it "handles 0% in distribution" do
      subject_experiment.rollout(described_class, distribution: [0, 100])

      allow(Zlib).to receive(:crc32).with(/\Agitlab_experiment_example:\h+\z/, nil).and_return(0)

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant2: 100)
    end
  end

  context "when distribution is specified as a hash" do
    it "rolls out with the expected distribution" do
      subject_experiment.rollout(described_class, distribution: { variant1: 75, variant2: 25 })

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant1: 75, variant2: 25)
    end

    it "handles 0% in distribution" do
      subject_experiment.rollout(described_class, distribution: { variant1: 0, variant2: 100 })

      allow(Zlib).to receive(:crc32).with(/\Agitlab_experiment_example:\h+\z/, nil).and_return(0)

      100.times { |i| run_cycle(subject_experiment, value: i) }

      expect(counts).to eq(variant2: 100)
    end
  end

  def run_cycle(experiment, **context)
    experiment.reset!(context)

    counts[experiment.assigned.name.to_sym] += 1
  end
end
