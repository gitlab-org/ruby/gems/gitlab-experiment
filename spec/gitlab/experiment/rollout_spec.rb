# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout do
  describe ".resolve" do
    it "resolves using a string" do
      result = described_class.resolve('gitlab/experiment/rollout/round_robin', foo: 'bar')

      expect(result).to be_a(Gitlab::Experiment::Rollout::Strategy)
      expect(result.klass).to be(Gitlab::Experiment::Rollout::RoundRobin)
      expect(result.options).to eq({ foo: 'bar' })
    end

    it "resolves using a symbol" do
      result = described_class.resolve(:random)

      expect(result.klass).to be(Gitlab::Experiment::Rollout::Random)
    end

    it "resolves using a class" do
      result = described_class.resolve(Gitlab::Experiment::Rollout::RoundRobin)

      expect(result.klass).to be(Gitlab::Experiment::Rollout::RoundRobin)
    end

    it "raises an exception on cases it can't resolve" do
      expect { config.default_rollout = [[:a]] }.to raise_error(
        ArgumentError,
        'unable to resolve rollout from [:a]'
      )

      expect { config.default_rollout = Gitlab::Experiment.new(:example) }.to raise_error(
        ArgumentError,
        /unable to resolve rollout from #<Gitlab::Experiment:0x/
      )

      expect { config.default_rollout = :a }.to raise_error(
        NameError,
        'uninitialized constant Gitlab::Experiment::Rollout::A'
      )

      expect { config.default_rollout = 'foo/bar' }.to raise_error(
        NameError,
        'uninitialized constant Foo'
      )
    end
  end

  describe Gitlab::Experiment::Rollout::Base do
    subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

    before do
      stub_experiments(example: true)

      subject_experiment.rollout(described_class)
      subject_experiment.variant(:variant1) {}
      subject_experiment.variant(:variant2) {}
    end

    it "delegates methods to the experiment" do
      expect(subject.cache).to be(subject_experiment.cache)
      expect(subject.cache).to be_an_instance_of(Gitlab::Experiment::Cache::Interface)
      expect(subject.id).to eq(subject_experiment.id)
    end

    it "validates when executing the rollout strategy" do
      expect_any_instance_of(described_class).to receive(:validate!)
      expect_any_instance_of(described_class).to receive(:execute_assignment)

      subject_experiment.assigned.name
    end

    it "returns the first behavior" do
      expect(subject_experiment.assigned.name).to eq(:variant1)
    end
  end
end
