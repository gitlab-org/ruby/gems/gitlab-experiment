# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Cookies do
  subject { Gitlab::Experiment::Context.new(subject_experiment) }

  let(:subject_experiment) { Gitlab::Experiment.new(:example) }
  let(:request) { double(headers: {}, cookie_jar: cookie_jar) }
  let(:cookie_jar) { double(delete: nil, signed: {}, permanent: double(signed: {})) }
  let(:cookie_name) { 'gitlab_experiment_example_id' }

  before do
    allow(SecureRandom).to receive(:uuid).and_return('abc123-456')
    allow(config).to receive(:cookie_domain).and_return('.gitlab.com')
  end

  it "sets a cookie value if needed" do
    subject.value(actor: nil, request: request)

    expect(subject.value[:actor]).to eq('abc123-456')
    expect(cookie_jar.permanent.signed[cookie_name]).to eq(
      domain: '.gitlab.com',
      httponly: true,
      secure: true,
      value: 'abc123-456'
    )
  end

  it "doesn't set a cookie value if there's already a value for it" do
    subject.value(actor: 42, request: request)

    expect(cookie_jar.permanent.signed[cookie_name]).to be_nil
  end

  it "doesn't create a migration if there's no cookie value" do
    expect(cookie_jar).to receive(:signed).and_return(cookie_name => nil)

    subject.value(actor: 42, request: request)

    expect(subject.signature[:migration_keys]).to be_nil
  end

  it "pulls in the existing cookie value" do
    expect(cookie_jar).to receive(:signed).and_return(cookie_name => 'xyz987-654')

    subject.value(actor: nil, request: request)

    expect(subject.value[:actor]).to eq('xyz987-654')
    expect(subject.signature).to eq(
      key: key_for(:example, actor: 'xyz987-654')
    )
  end

  it "deletes and migrates from the cookie value if we can" do
    expect(cookie_jar).to receive(:signed).and_return(cookie_name => 'xyz987-654')
    expect(cookie_jar).to receive(:delete).with(cookie_name, domain: '.gitlab.com')

    subject.value(actor: 42, request: request)

    expect(subject.signature).to eq(
      key: key_for(:example, actor: 42),
      migration_keys: [key_for(:example, actor: 'xyz987-654')]
    )
  end

  it "allows cookie name override" do
    allow(config).to receive(:cookie_name).and_return(->(experiment) { "x_#{experiment.name}" })

    subject.value(actor: nil, request: request)

    expect(subject.value[:actor]).to eq('abc123-456')
    expect(cookie_jar.permanent.signed["x_gitlab_experiment_example"]).to eq(
      domain: '.gitlab.com',
      httponly: true,
      secure: true,
      value: 'abc123-456'
    )
  end
end
