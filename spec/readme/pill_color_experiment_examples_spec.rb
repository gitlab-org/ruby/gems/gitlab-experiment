# frozen_string_literal: true

require 'spec_helper'

# Our README.md file provides examples of an advanced experiment, and we want to test some of the code that we provide
# in our documentation against an actual implementation of this example expeirment.
class PillColorExperiment < Gitlab::Experiment
  # Register our behaviors.
  control # register our control, which will by default call #control_behavior
  variant(:red) # register the red variant that will call #red_behavior
  variant(:blue) # register the blue variant that will call #blue_behavior

  # Exclude any users that are named "Richard".
  exclude :users_named_richard

  # Segment any account older than 2 weeks into the red variant without asking
  # the rollout strategy to assign a variant.
  segment :old_account?, variant: :red

  # After the experiment has been run, we want to log some performance metrics.
  after_run { log_performance_metrics }

  private

  # Define the default control behavior, which can be overridden on runs.
  def control_behavior
    'grey'
  end

  # Define the default red behavior, which can be overridden on runs.
  def red_behavior
    'red'
  end

  # Define the default blue behavior, which can be overridden on runs.
  def blue_behavior
    'blue'
  end

  # Define our special exclusion logic.
  def users_named_richard
    context.try(:actor)&.first_name == 'Richard' # use try for nil actors
  end

  # Define our segmentation logic.
  def old_account?
    context.try(:actor) && context.actor.created_at < 2.weeks.ago
  end

  # Let's assume that we've tracked this and want to push it into some system.
  def log_performance_metrics
    # ...hypothetical implementation
  end
end

RSpec.describe "The pill color experiment documentation" do
  it "ensures the playground examples are correct" do
    ex = experiment(:pill_color)

    expect(ex.run).to eq('grey')
    expect(ex.track(:clicked)).to be_truthy
    expect(ex.publish).to include(
      experiment: 'gitlab_experiment_pill_color',
      variant: 'control',
      key: 'e2a7453352778af1b23bb0e94161465013e404d5112793685cb140ae833756a7',
      excluded: false
    )
  end

  describe "Testing (rspec support)", :experiment do
    describe "Stub helpers" do
      it "stubs experiments to resolve to a specific variant" do
        stub_experiments(pill_color: :red)

        experiment(:pill_color) do |e|
          expect(e).to be_enabled
          expect(e.assigned.name).to eq(:red)
        end
      end

      it "stubs experiments while allowing the rollout strategy to assign the variant" do
        stub_experiments(pill_color: true) # only stubs enabled?

        experiment(:pill_color) do |e|
          expect(e.rollout).to receive(:resolve).and_return(:blue)

          expect(e).to be_enabled
          expect(e.assigned.name).to eq(:blue)
        end
      end
    end

    describe "Registered behaviors matcher" do
      it "tests our registered behaviors" do
        expect(experiment(:pill_color)).to register_behavior(:control)
          .with('grey') # with a return value of "grey"
        expect(experiment(:pill_color)).to register_behavior(:red)
        expect(experiment(:pill_color)).to register_behavior(:blue)
      end
    end

    describe "Exclusion and segmentation matchers" do
      let(:excluded) { double(first_name: 'Richard', created_at: Time.current) }
      let(:segmented) { double(first_name: 'Jeremy', created_at: 3.weeks.ago) }

      it "tests the exclusion rules" do
        expect(experiment(:pill_color)).to exclude(actor: excluded)
        expect(experiment(:pill_color)).not_to exclude(actor: segmented)
      end

      it "tests the segmentation rules" do
        expect(experiment(:pill_color)).to segment(actor: segmented)
          .into(:red) # into a specific variant
        expect(experiment(:pill_color)).not_to segment(actor: excluded)
      end
    end

    describe "Tracking matcher" do
      before do
        stub_experiments(pill_color: true) # stub the experiment so tracking is permitted
      end

      it "tests that we track an event on a specific instance" do
        expect(subject = experiment(:pill_color)).to track(:clicked)

        subject.track(:clicked)
      end

      it "tests that we track an event with specific details" do
        expect(experiment(:pill_color)).to track(:clicked, value: 1, property: '_property_')
          .on_next_instance # any time in the future
          .with_context(foo: :bar) # with the expected context
          .for(:red) # and assigned the correct variant

        experiment(:pill_color, :red, foo: :bar).track(:clicked, value: 1, property: '_property_')
      end
    end
  end
end
