# frozen_string_literal: true

require 'spec_helper'
require 'flipper'

module Gitlab::Experiment::Rollout
  class Flipper < Percent
    def enabled?
      ::Flipper.enabled?(experiment.name.tr('/', '_'), self)
    end

    def flipper_id
      "Experiment;#{id}"
    end
  end
end

RSpec.describe "The flipper rollout strategy documentation" do
  subject_experiment_class = Class.new(Gitlab::Experiment) do
    control {}
    variant(:red) {}
    variant(:blue) {}

    default_rollout :flipper, distribution: { control: 10, red: 30, blue: 60 }
  end

  it "works as described with flipper" do
    expect(subject_experiment_class.new('namespaced/example')).not_to be_enabled

    Flipper.enable(:gitlab_experiment_namespaced_example)

    expect(subject_experiment_class.new('namespaced/example')).to be_enabled
  end

  it "honors the distribution rules" do
    Flipper.enable(:gitlab_experiment_example)

    subject_experiment = subject_experiment_class.new(:example)
    counts = { control: 0, red: 0, blue: 0 }

    100.times do |i|
      subject_experiment.reset!(value: i)

      counts[subject_experiment.assigned.name.to_sym] += 1
    end

    expect(counts).to eq(control: 5, red: 30, blue: 65)
  end
end
