# frozen_string_literal: true

module Danger
  class ChangelogReminder < Danger::Changelog
    def categories_need_changelog?
      true
    end
  end
end
