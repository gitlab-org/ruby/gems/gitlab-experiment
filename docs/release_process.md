# Release Process

## Versioning

We follow [Semantic Versioning](https://semver.org).  In short, this means that the new version should reflect the types of changes that are about to be released.

*summary from semver.org*

MAJOR.MINOR.PATCH

- MAJOR version when you make incompatible API changes,
- MINOR version when you add functionality in a backwards compatible manner, and
- PATCH version when you make backwards compatible bug fixes.

## When we release

We release `gitlab-experiment` on an ad-hoc basis. There is no regularity to when we release, we simply release when the changes warrant a new release. This can be to fix a bug, or to implement new features and/or deprecate existing ones.

[Automated gem release process](https://gitlab.com/gitlab-org/quality/pipeline-common#release-process) is used to release new version of `gitlab-experiment` through [pipelines](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/blob/6d2b055620bdf57cf2536616f570aca1e65e6322/.gitlab-ci.yml#L28), and this will:

- Create a new [release](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/releases) with a new [tag](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/tags) in the `gitlab-experiment` project.
- Publish the gem: https://rubygems.org/gems/gitlab-experiment
- Populate the release log with the API contents. For example: https://gitlab.com/api/v4/projects/21250701/repository/changelog?version=0.9.1

We follow this release process in a separate merge request from the one that introduced the changes. The release merge request should just contain a version bump.

### Testing unreleased changes in merge requests

To test an unreleased change in an actual merge request, you can create a merge request that will install the unreleased version of `gitlab-experiment`. Bundler can install gems by specifying a repository and a revision from Git.

For example, to test `gitlab-experiment` changes from the `your-branch-name` branch in [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab), in the `Gemfile`:

```ruby
gem 'gitlab-experiment', '~> 0.9.1',
  git: 'https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment.git',
  ref: 'your-branch-name'
```

### Before release

Changes merged since the last release should have had changelog entries (see [Contributing](#contributing)).

If changelog entries are missing, you can also edit the release notes after it's being released.

### Steps to release

Use a `Release` merge request [template](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/blob/master/.gitlab/merge_request_templates/Release.md) and create a merge request to update the version number in `version.rb`, and get the merge request merged by a maintainer.

This will then be packaged into a gem and pushed to [rubygems.org](https://rubygems.org) by the CI/CD.

## Contributing

Bug reports and merge requests are welcome at https://gitlab.com/gitlab-org/gitlab-experiment. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/gitlab-org/ruby/gems/gitlab-experiment/-/blob/master/CODE_OF_CONDUCT.md).

Make sure to include a changelog entry in your commit message and read the [changelog entries section](https://docs.gitlab.com/ee/development/changelog.html).

